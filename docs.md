# Types
A variable will have a type after it.
"Hex" means a string encoded in ASCII 0-F, it is 50% space efficient.
"RSA Key" means a base64 string encased in -----BEGIN RSA PUBLIC/PRIVATE KEY-----


# data (hex) toHex(binary (string))
Unpacks the binary data into safely printable hexadecimal data.
Do not save data in this format as it is inefficient, it is meant for frontend.

# binary (string) fromHex(data (hex))
Packs the hexadecimal data into binary.
Useful for saving and loading as it does not waste any space.
Returns nil if the string is not hexadecimal.

# hash (hex) SHA{224, 256, 384, 512}(data (string))
Calculates a SHA-2 cryptographic hash of the data.
Returns it in hexadecimal format.

# encrypted (hex) encAES256(data (string), key (string))
Encrypts the data using an AES-256 key.
Returns the encrypted data in hexadecimal format.

# decrypted (string) decAES256(encrypted (hex), key (string))
Decrypts data encrypted with encAES256().
Returns nil if the data was not encrypted with the key.

# public (RSA Key), private (RSA Key) genRSA(bitSize (number) - one of {512, 1024, 2048, 4096, 8192}, password (string) [optional])
Generates an RSA public and private key pair.
You can supply it with a password to make it stronger.

# encrypted (hex) encRSA(bitSize (number) - same as genRSA, data (string), public (RSA Key))
Encrypts the data using an RSA public key.
The public key is generated with genRSA{keyLengthInBits}().
Returns nil if the public key is of the wrong length.
Returns the encrypted data in hexadecimal format.

# decrypted (string) decRSA(bitSize (number) - same as genRSA, encrypted (hex), private (RSA Key))
Decrypts the data using an RSA private key.
The public key is generated with genRSA{keyLengthInBits}().
Returns nil if the private key is of the wrong length, or if the data was not encrypted with its matching public key.