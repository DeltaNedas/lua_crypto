BUILDDIR := build
OBJECTDIR := objects
SOURCEDIR := src

# Options, use make KEY=VALUE
LUA ?= 5.4

CC := gcc
CCFLAGS := -I/usr/include/lua$(LUA) -O3 -Wall -ansi -pedantic -std=c99 -c -fPIC -g
LDFLAGS := -shared -lcrypto -llua$(LUA)

LIBRARY := crypto
SOURCES := $(wildcard $(SOURCEDIR)/*.c)
OBJECTS := $(patsubst $(SOURCEDIR)/%, $(OBJECTDIR)/%.o, $(SOURCES))

BINARIES := /usr/local/lib/lua/$(LUA)

all: $(LIBRARY)

install:
	cp -f $(BUILDDIR)/$(LIBRARY).so $(BINARIES)/

uninstall:
	rm -f $(BINARIES)/$(LIBRARY).so

$(OBJECTDIR)/%.c.o: $(SOURCEDIR)/%.c
	@mkdir -p `dirname $@`
	$(CC) $(CCFLAGS) -o $@ $^

$(LIBRARY): $(OBJECTS)
	@mkdir -p $(BUILDDIR)
	$(CC) -o $(BUILDDIR)/$@.so $^ $(LDFLAGS)

clean:
	rm -rf $(OBJECTDIR)
	rm -rf $(BUILDDIR)
