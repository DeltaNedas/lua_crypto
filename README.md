# lua_crypto
A Lua module written in C that adds cryptography functions such as hashing and encryption.
It acts mostly as a wrapper for OpenSSL.
All functions take in or out a hexadecimal string.

# Variety
Hash functions:
* SHA-224
* SHA-256
* SHA-384
* SHA-512

Asymmetric encryption:
* RSA-2048
* RSA-4096
* RSA-8192

Symmetric encryption:
* AES-256

# How to use?
1. `make`
2. `sudo make install` or `cd build`
3. `> local crypto = require("crypto")`
See **examples/test.lua**.

# License
Lua_crypto is licensed under the MIT License.
