#!/usr/bin/env lua
local crypto = require("crypto")

local input = assert(arg[1], "Run with a string to hash.")
local hash = crypto.SHA256(input)

print("Hash of string is "..hash..".")

local public, private = crypto.genRSA8192()
local encrypted = crypto.encRSA8192(input, public)

print("Encrypted string is "..encrypted..".")

local decrypted = crypto.decRSA8192(input, private)
local decHash = crypto.SHA256(decrypted)

print("Decrypted's hash is "..decHash..".")
if hash ~= decHash then
	print("Something went wrong and their hashes are different!")
	os.exit(1)
end
