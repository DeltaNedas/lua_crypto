/*
Copyright 2019 (c) DeltaNedas

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <openssl/err.h>
#include <openssl/evp.h> // AES and RSA
#include <openssl/pem.h>
#include <openssl/rand.h>
#include <openssl/rsa.h>
#include <openssl/sha.h> // SHA
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

struct rsaKeys {
	char* public;
	char* private;
};

char* toHex(lua_State* L, unsigned char* binary, unsigned long len) {
	char* hex = malloc(len * sizeof(char) * 2);
	if (hex == NULL) {
		luaL_error(L, "Failed to allocate memory for hex data: %s", strerror(errno));
		return NULL;
	}

	for (int i = 0; i < len; i++) {
		printf("Binary is %c\n", binary[i]);
		sprintf(&hex[i * 2], "%02X", binary[i]);
	}
	printf("Hex is %s\n", hex);
	printf("Hex is %ld bytes.\n", len * 2);
	return hex;
}

unsigned char* fromHex(lua_State* L, char* hex) {
	unsigned long len = strlen(hex) / 2;
	unsigned char* binary = malloc((len + 1) * sizeof(char));
	if (binary == NULL) {
		luaL_error(L, "Failed to allocate memory for binary data: %s", strerror(errno));
		return NULL;
	}

	for (int i = 0; i < len; i++) {
		binary[i] = (hex[i * 2] % 32 + 9) % 25 * 16 + (hex[i * 2 + 1] % 32 + 9) % 25;
	}
	binary[len] = '\0';
	return binary;
}

char* sha224(lua_State* L, const char* str) {
	unsigned char* hash = malloc(SHA224_DIGEST_LENGTH * sizeof(char));
	SHA256_CTX context;
	SHA224_Init(&context);
	SHA224_Update(&context, str, strlen(str));
	SHA224_Final(hash, &context);

	char* hex = toHex(L, hash, SHA224_DIGEST_LENGTH);
	free(hash);
	return hex;
}
char* sha256(lua_State* L, const char* str) {
	unsigned char* hash = malloc(SHA256_DIGEST_LENGTH * sizeof(char));
	SHA256_CTX context;
	SHA256_Init(&context);
	SHA256_Update(&context, str, strlen(str));
	SHA256_Final(hash, &context);

	char* hex = toHex(L, hash, SHA256_DIGEST_LENGTH);
	free(hash);
	return hex;
}
char* sha384(lua_State* L, const char* str) {
	unsigned char* hash = malloc(SHA384_DIGEST_LENGTH * sizeof(char));
	SHA512_CTX context;
	SHA384_Init(&context);
	SHA384_Update(&context, str, strlen(str));
	SHA384_Final(hash, &context);

	char* hex = toHex(L, hash, SHA384_DIGEST_LENGTH);
	free(hash);
	return hex;
}
char* sha512(lua_State* L, const char* str) {
	unsigned char* hash = malloc(SHA512_DIGEST_LENGTH * sizeof(char));
	SHA512_CTX context;
	SHA512_Init(&context);
	SHA512_Update(&context, str, strlen(str));
	SHA512_Final(hash, &context);

	char* hex = toHex(L, hash, SHA512_DIGEST_LENGTH);
	free(hash);
	return hex;
}

char* encAES256(lua_State* L, const char* str, const char* key, const char* iv) {
	OpenSSL_add_all_algorithms();
	ERR_load_crypto_strings();
	EVP_CIPHER_CTX* context = EVP_CIPHER_CTX_new();
	if (context == NULL) {
		return NULL;
	}

	EVP_EncryptInit_ex(context, EVP_aes_256_gcm(), NULL, NULL, NULL);
	EVP_EncryptInit_ex(context, NULL, NULL, (unsigned char*) toHex(L, (unsigned char*) key, strlen(key)), NULL); // TODO: Actual IV support

	unsigned char* enc;
	int len;
	EVP_EncryptUpdate(context, enc, &len, (unsigned char*) str, strlen(str));
	EVP_EncryptFinal_ex(context, enc + len, &len);
	EVP_CIPHER_CTX_free(context);

	printf("woooo %s\n", enc);

	char* hex = toHex(L, enc, len);
	free(enc);
	ERR_free_strings();
	return hex;
}

struct rsaKeys* genRSA(unsigned int bits, unsigned int exponent, const char* password) {
	OpenSSL_add_all_algorithms();
#ifdef _WIN32
	char rand_buff[16];
	RAND_seed(rand_buff, 16);
#else
	RAND_load_file("/dev/urandom", 1024);
#endif

	if (exponent == 0) {
		exponent = 65537;
	}

	BIO* publicBio = NULL;
	BIO* privateBio = NULL;
	BIGNUM* e = NULL;
	RSA* context = RSA_new();
	if (context == NULL) {
		printf("context die\n");
		goto quit;
	}

	e = BN_new();
	if (BN_set_word(e, RSA_F4) != 1) {
		printf("number die\n");
		goto quit;
	}

	if (RSA_generate_key_ex(context, bits, e, NULL) != 1) {
		printf("context key die\n");
		goto quit;
	}

	char* publicKey;
	char* privateKey;

	publicBio = BIO_new(BIO_s_mem());
	if (PEM_write_bio_RSAPublicKey(publicBio, context) != 1) {
		printf("private key die\n");
		goto quit;
	}
	unsigned int publicLen = BIO_pending(publicBio);
	publicKey = calloc(publicLen + 1, 1); /* Null-terminate */
	BIO_read(publicBio, publicKey, publicLen);

	privateBio = BIO_new(BIO_s_mem());
	if (PEM_write_bio_RSAPrivateKey(privateBio, context, NULL, NULL, 0, NULL, NULL) != 1) {
		printf("private key die\n");
		goto quit;
	}

	unsigned int privateLen = BIO_pending(privateBio);
	privateKey = calloc(privateLen + 1, 1); /* Null-terminate */
	BIO_read(privateBio, privateKey, privateLen);

	BIO_free_all(publicBio);
	BIO_free_all(privateBio);
	BN_free(e);
	RSA_free(context);

	struct rsaKeys* keys;
	keys->public = publicKey;
	keys->private = privateKey;
	printf("no?\n");
	return keys;

	quit:
		BIO_free_all(publicBio);
		BIO_free_all(privateBio);
		BN_free(e);
		RSA_free(context);
		return NULL;
}


int crypto_SHA224(lua_State* L) {
	const char* data = luaL_checkstring(L, 1);
	char* hash = sha224(L, data);
	lua_pushstring(L, hash);
	return 1;
}
int crypto_SHA256(lua_State* L) {
	const char* data = luaL_checkstring(L, 1);
	char* hash = sha256(L, data);
	lua_pushstring(L, hash);
	return 1;
}
int crypto_SHA384(lua_State* L) {
	const char* data = luaL_checkstring(L, 1);
	char* hash = sha384(L, data);
	lua_pushstring(L, hash);
	return 1;
}
int crypto_SHA512(lua_State* L) {
	const char* data = luaL_checkstring(L, 1);
	char* hash = sha512(L, data);

	lua_pushstring(L, hash);
	return 1;
}

int crypto_encAES256(lua_State* L) {
	const char* data = luaL_checkstring(L, 1);
	const char* key = luaL_checkstring(L, 2);
	const char* iv = lua_tostring(L, 3);
	char* enc = encAES256(L, data, key, iv);
	if (enc == NULL) {
		lua_pushnil(L);
		lua_pushstring(L, ERR_error_string(ERR_get_error(), NULL));
		return 2;
	}

	lua_pushstring(L, enc);
	return 1;
}

int crypto_genRSA(lua_State* L) {
	struct rsaKeys* keys = genRSA(luaL_checknumber(L, 1), lua_tonumber(L, 3), lua_tostring(L, 2));
	if (keys == NULL) {
		printf("sadness\n");
		lua_pushnil(L);
		lua_pushnumber(L, ERR_get_error());
		return 2;
	}
	lua_pushstring(L, keys->public);
	lua_pushstring(L, keys->private);
	return 2;
}


static struct luaL_Reg crypto_lib[] = {
	{"SHA224", crypto_SHA224},
	{"SHA256", crypto_SHA256},
	{"SHA384", crypto_SHA384},
	{"SHA512", crypto_SHA512},
	{"encAES256", crypto_encAES256},
	//{"decAES256", crypto_decAES256},
	{"genRSA", crypto_genRSA},
	{NULL, NULL} // sentinel
};

int luaopen_crypto(lua_State* L) {
	luaL_newlib(L, crypto_lib);
	return 1;
}
